#include <iostream>;
using namespace std;


class Animal
{
public:
	virtual int Voice()
	{
		cout << "animal";
		return 0;
	}

};

class Dog : public Animal
{
public:
	int Voice() override
	{
		cout << "Dog: ";
		cout << "Wow!";
		return 0;
	}
};

class Cat : public Animal 
{
public:
	int Voice() override
	{
		cout << "Cat: ";
		cout << "Meow!";
		return 0;
	}
};

class Frog : public Animal 
{
public:
	int Voice() override
	{
		cout << "Frog: ";
		cout << "CwaCwa!";
		return 0;
	}
};

int main()
{
	Animal* array[3] = { new Dog, new Cat, new Frog};
	for (int i =0 ; i < 3; i++)
	{
		cout << array[i]->Voice() << endl;
	}
}
